<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: MAKSIM
  Date: 17.07.13
  Time: 12:19
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        function doAjaxPost(index) {
            //alert(index);
            // get the form values
            $.ajax({
                type:   "POST",
                url:    "/published",
                data:   "id=" + index,
                success: function(response){
                        $('#publish_'+index).html(response);
                },
                error: function(e){
                    alert('Error: ' + e);
                }
            });
        }
    </script>

    <title></title>
</head>
<body>
    <div id="model">
        <jsp:include page="_model.jsp"/>
    </div>
    <div id="form">
        <jsp:include page="_form.jsp"/>
    </div>
    <a href="/letter"><spring:message code="letter.update"/></a>
</body>
</html>