package ru.mxelgin.test.repository;

import org.hibernate.Query;
import ru.mxelgin.test.models.Letter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: MAKSIM
 * Date: 18.07.13
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */
public interface LetterRepository {

    public void addLetter(Letter letter);

    public void updateLatter(Letter letter);

    public List<Letter> listLetters();

    public void removeLetter(Integer id);

    public Letter editLetter(Integer id);
}
