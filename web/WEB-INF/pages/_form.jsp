<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--<div id="info" style="color: green;">--%>
    <%--<jsp:include page="_model.jsp"/>--%>
${test}
<c:if test="${!empty listLetters}">
    <table class="data">
        <tr>
            <td><spring:message code="letter.id"/></td>
            <td><spring:message code="letter.number"/></td>
            <td><spring:message code="letter.date"/></td>
            <td><spring:message code="letter.topic"/></td>
            <td><spring:message code="letter.published"/></td>
            <td><spring:message code="letter.scan"/></td>
            <td><spring:message code="letter.comments"/></td>
            <td><spring:message code="letter.edit"/></td>
            <td><spring:message code="letter.delete"/></td>
            <td><spring:message code="letter.view"/></td>
            <td><spring:message code="letter.download"/></td>
        </tr>
        <c:forEach items="${listLetters}" var="letter">
            <tr>
                <td>${letter.id}</td>
                <td>${letter.number}</td>
                <td><fmt:formatDate value="${letter.date}" pattern="dd-MM-yyyy" /></td>
                <td>${letter.topic}</td>
                <td id="publish_${letter.id}">${letter.published}</td>
                <td>${letter.scanname}</td>
                <td>${letter.comments}</td>
                <td><a href="/edit/${letter.id}"><spring:message code="letter.edit"/></a></td>
                <td><a href="/delete/${letter.id}"><spring:message code="letter.delete"/></a></td>
                <td><a href="/view/${letter.id}"><spring:message code="letter.view"/></a></td>
                <td><a href="/download/${letter.id}"><spring:message code="letter.download"/></a></td>
                <td><a href="/edit/${letter.id}"><spring:message code="letter.edit"/></a></td>
                <td><a href="#" onclick="doAjaxPost(${letter.id});"><spring:message code="letter.publish"/></a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<%--</div>--%>


