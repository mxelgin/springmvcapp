package ru.mxelgin.test.models;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: MAKSIM
 * Date: 11.07.13
 * Time: 19:48
 * To change this template use File | Settings | File Templates.
 */
@Table(name = "letter", schema = "", catalog = "letters")
@Entity
public class Letter {
    private CommonsMultipartFile file = null;
    private int id;
    private String number;
    private Timestamp date;
    private String topic;
    private Boolean published;
    private byte[] scan;
    private String comments;
    private String scanname;

    @Transient
    public CommonsMultipartFile getFile() {
        return file;
    }

    public void setFile(CommonsMultipartFile file) {
        if (file != null)
            if (!file.isEmpty()){
                this.file = file;
                this.scan = file.getBytes();//.getOriginalFilename();
                this.scanname = file.getOriginalFilename();
            }
    }

    @Column(name = "id", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "number", nullable = true, insertable = true, updatable = true, length = 20, precision = 0)
    @Basic
    @NotEmpty
    @Size(min=1, max=20)
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Basic
    @Column(name = "date", nullable = false, insertable = true, updatable = true, length = 19, precision = 0)
//    @Basic
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @NotNull
//    @Temporal(value=TemporalType.DATE)
//    @DateTimeFormat(pattern="MM/dd/yyyy")
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Column(name = "topic", nullable = false, insertable = true, updatable = true, length = 250, precision = 0)
    @Basic
    @NotEmpty
    @Size(min=1, max=250)
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Column(name = "published", nullable = true, insertable = true, updatable = true, length = 1, precision = 0)
    @Basic
    public Boolean getPublished() {
        return published;
    }

    public void setPublished(Boolean published) {
        this.published = (published == null?false:published);
    }

    @Column(name = "scan", nullable = false, insertable = true, updatable = true, length = 2147483647, precision = 0)
    @Basic
//    @NotEmpty
    public byte[] getScan() {
        return scan;
    }

    public void setScan(byte[] scan) {
        this.scan = scan;
    }

    @Column(name = "scanname", nullable = false, insertable = true, updatable = true, length = 20, precision = 0)
    @Basic
    @Size(max=20)
//    @NotEmpty
    public String getScanname() {
        return scanname;
    }

    public void setScanname(String scanname) {
        this.scanname = scanname;
    }

    @Column(name = "comments", nullable = true, insertable = true, updatable = true, length = 65535, precision = 0)
    @Basic
    @Size(min=0, max=1000)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Letter that = (Letter) o;

        if (id != that.id) return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (number != null ? !number.equals(that.number) : that.number != null) return false;
        if (published != null ? !published.equals(that.published) : that.published != null) return false;
        if (!Arrays.equals(scan, that.scan)) return false;
        if (topic != null ? !topic.equals(that.topic) : that.topic != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (topic != null ? topic.hashCode() : 0);
        result = 31 * result + (published != null ? published.hashCode() : 0);
        result = 31 * result + (scan != null ? Arrays.hashCode(scan) : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }



}
