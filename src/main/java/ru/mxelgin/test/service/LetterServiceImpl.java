package ru.mxelgin.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mxelgin.test.models.Letter;
import ru.mxelgin.test.repository.LetterRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: MAKSIM
 * Date: 11.07.13
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */
@Service
public class LetterServiceImpl implements LetterService{

    @Autowired
    private LetterRepository letterRepository;

    @Transactional
    public void addLetter(Letter letter){
        letterRepository.addLetter(letter);
    }

    @Transactional
    public void updateLetter(Letter letter){
        letterRepository.updateLatter(letter);
    }

    @Transactional
    public List<Letter> listLetters(){
        return letterRepository.listLetters();
    }

    @Transactional
    public void removeLetter(Integer id){
        letterRepository.removeLetter(id);
    }

    @Transactional
    public Letter editLetter(Integer id) {
        return letterRepository.editLetter(id);
    }
}
