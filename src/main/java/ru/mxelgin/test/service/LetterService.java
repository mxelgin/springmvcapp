package ru.mxelgin.test.service;

import org.springframework.transaction.annotation.Transactional;
import ru.mxelgin.test.models.Letter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: MAKSIM
 * Date: 18.07.13
 * Time: 11:30
 * To change this template use File | Settings | File Templates.
 */
public interface LetterService {
    public void addLetter(Letter letter);

    public void updateLetter(Letter letter);

    public List<Letter> listLetters();

    public void removeLetter(Integer id);

    public Letter editLetter(Integer id);

}
