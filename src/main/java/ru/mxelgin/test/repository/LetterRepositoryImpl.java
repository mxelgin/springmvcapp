package ru.mxelgin.test.repository;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ru.mxelgin.test.models.Letter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: MAKSIM
 * Date: 11.07.13
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */

/*
LetterDAOImpl
правильное название

*/
@Repository
public class LetterRepositoryImpl implements LetterRepository{
//    Letter entity;
    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    public void addLetter(Letter letter){
        sessionFactory.getCurrentSession().saveOrUpdate(letter);
    }

    public void updateLatter(Letter letter){
        sessionFactory.getCurrentSession().update(letter);
    }

    public List<Letter> listLetters(){
        return sessionFactory.getCurrentSession().createQuery("from Letter order by date desc").list();
    }

    public void removeLetter(Integer id){

        Letter letter = (Letter)sessionFactory.getCurrentSession().load(Letter.class, id );
        if (letter != null){
            sessionFactory.getCurrentSession().delete(letter);
        }
    }

    @SuppressWarnings("unchecked")
    public Letter editLetter(Integer id){
        Query query = sessionFactory.getCurrentSession().createQuery("from Letter where id = " + id);
        if (query == null) return  null;
        List list = query.list();
        if (list.size() == 0) return null;
        return (Letter)query.list().get(0);
//        Session session = sessionFactory.getCurrentSession();
//        this.entity = (Letter) session.load(Letter.class, id);
//
//        return  this.entity;

    }
}
