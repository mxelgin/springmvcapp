/**
 * Created with IntelliJ IDEA.
 * User: MAKSIM
 * Date: 07.11.13
 * Time: 11:21
 * To change this template use File | Settings | File Templates.
 */
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.util.Properties;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    static {
        try {
             // Create the SessionFactory from hibernate.cfg.xml
            Configuration configuration = new Configuration();
            //configuration.addResource("test.java");
            configuration.configure("test.hibernate.cfg.xml");
            Properties properties = configuration.getProperties();
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(properties).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}