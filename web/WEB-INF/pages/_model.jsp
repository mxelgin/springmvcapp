<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <form:form action="/add"  modelAttribute="letter" enctype="multipart/form-data">
        <table>
            <%--<tr>--%>
            <%--<td><form:label path="id"><spring:message code="letter.id"/></form:label></td>--%>
            <%--<td><form:input path="id" readonly="true"/></td>--%>
            <%--<td><form:errors path="id" cssclass="error"/></td>--%>
            <%--</tr>--%>
            <tr>
                <td>
                    <spring:bind path="letter.id">
                        <input id="id" type="hidden" name="id" value="${letter.id}"/>
                    </spring:bind>
                </td>
                <td><form:errors path="id" cssclass="error"/></td>
            </tr>
            <tr>
                <td><form:label path="number"><spring:message code="letter.number"/></form:label></td>
                <td><form:input id="number" path="number"/></td>
                <td><form:errors path="number" cssClass="error"/></td>
            </tr>
            <tr>
                <td><form:errors path="date" cssClass="error"/></td>
            </tr>
            <%--<tr>--%>
            <%--<td><form:label path="date">DATE</form:label></td>--%>
            <%--<td><form:input path="date"/></td>--%>
            <%--<td><form:errors path="date" cssClass="error"/></td>--%>
            <%--</tr>--%>
            <tr>
                <td><form:label path="topic"><spring:message code="letter.topic"/></form:label></td>
                <td><form:input path="topic"/></td>
                <td><form:errors path="topic" cssClass="error"/></td>
            </tr>
            <%--<tr>--%>
                <%--<td><form:label path="published"><spring:message code="letter.published"/></form:label></td>--%>
                <%--<td><form:checkbox path="published"/></td>--%>
                <%--<td><form:errors path="published" cssClass="error"/></td>--%>
            <%--</tr>--%>
            <tr>
                <td><form:label path="published"><spring:message code="letter.published"/></form:label></td>
                <td>
                    <spring:bind path="letter.published">
                        <input id="published" type="checkbox" name="published" ${letter.published==true?"checked":""}/>
                    </spring:bind>
                </td>
                <td><form:errors path="published" cssClass="error"/></td>
            </tr>
            <%--<tr>--%>
            <%--<td><form:label path="scan"><spring:message code="letter.scan"/></form:label></td>--%>
            <%--<td><form:input  path="scan"/></td>--%>
            <%--<td><form:errors path="scan" cssClass="error"/></td>--%>
            <%--</tr>--%>
            <%--<tr>--%>
            <%--<td>--%>
            <%--<spring:bind path="letter.scan">--%>
            <%--<input type="text" name="scan" value="${letter.scan}"/>--%>
            <%--</spring:bind>--%>
            <%--</td>--%>
            <%--</tr>--%>
            <%--<tr>--%>
            <%--<td><form:label path="scanname"><spring:message code="letter.scanname"/></form:label></td>--%>
            <%--<td><form:input  id="scanname" path="scanname"/></td>--%>
            <%--<td><form:errors path="scanname" cssClass="error"/></td>--%>
            <%--</tr>--%>
            <tr>
                <td><form:label path="file"></form:label></td>
                <td>
                    <spring:bind path="letter.file">
                        <input type="file" name="file" value="${letter.file.originalFilename}" onchange="scanname.value = this.value.replace(/^.*[\\\/]/, '');"/>
                    </spring:bind>
                </td>
                <td><form:errors path="file" cssClass="error"/></td>
                <td><form:errors path="scanname" cssClass="error"/></td>
                <td><form:errors path="scan" cssClass="error"/></td>
            </tr>
            <tr>
                <td><form:label path="comments"><spring:message code="letter.comments"/></form:label></td>
                <td><form:input path="comments"/></td>
                <td><form:errors path="comments" cssClass="error"/></td>
            </tr>
            <tr>
                <td colspan="3"><input type="submit" value="<spring:message code="letter.save"/>"/></td>
            </tr>
        </table>
    </form:form>
<%--<input type="button" value="Add Users" onclick="doAjaxPost()">--%>

