package ru.mxelgin.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.mxelgin.test.models.LetterValidator;
import ru.mxelgin.test.models.Letter;
import ru.mxelgin.test.service.LetterService;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: MAKSIM
 * Date: 10.07.13
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/")
public class AppLetterController {
//
    private LetterValidator countryValidator = new LetterValidator();
    @Autowired
    private LetterService letterService;

    @RequestMapping(value = "/published", method = RequestMethod.POST)
    public @ResponseBody
    String publishLetter(Integer id){
        Letter letter = letterService.editLetter(id);
        if (letter.getPublished() == true) return "true";

        letter.setPublished(true);
        letterService.updateLetter(letter);
        return "true";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String addLetter(@Valid Letter letter, BindingResult result, ModelMap model){
        //System.out.println(letter);
        countryValidator.Validate(letter, letterService, result);
        if (result.hasErrors()){
            model.addAttribute("listLetters", letterService.listLetters());
            return "view";
        }

        letterService.addLetter(letter);
        return "redirect:/letter";
    }

    @RequestMapping("/letter")
    public String listLetters(Model model){
//        map.addAttribute("action", "/add");
        model.addAttribute("message", "Hello world!");
        model.addAttribute("letter", new Letter());
        model.addAttribute("listLetters", letterService.listLetters());
        return "view";
    }

    @RequestMapping("/delete/{letterId}")
    public String deleteLetter(@PathVariable("letterId") Integer letterId) {
        letterService.removeLetter(letterId);
        return "redirect:/view";
    }

    @RequestMapping("/edit/{letterId}")
    public String editLetter(@PathVariable("letterId") Integer letterId, ModelMap model){
//        map.put("action", "/update");
        model.addAttribute("letter", letterService.editLetter(letterId));
        model.addAttribute("listLetters", letterService.listLetters());
        return "view";
    }

    @RequestMapping("/")
    public String home() {
        return "redirect:/letter";
    }

    @RequestMapping("/download/{letterId}")
    public void downloadScan(@PathVariable("letterId") Integer letterId, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
        Letter letter = letterService.editLetter(letterId);
        BufferedInputStream filein = null;
        BufferedOutputStream outputs = null;
        try {
            InputStream in = new ByteArrayInputStream(letter.getScan());//sb.toString().getBytes("UTF-8"));
            ServletOutputStream out = response.getOutputStream();
            byte[] outputByte = new byte[4096];
//copy binary contect to output stream
            int len = 0;
            response.setHeader("Content-Length", ""+ letter.getScan().length);
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition","attachment;filename="+ letter.getScanname());
            response.setHeader("Content-Transfer-Encoding", "binary");
            while((len = in.read(outputByte)) > 0)
            {
                out.write(outputByte, 0, len);
                out.flush();
            }
            in.close();
            out.close();
        }
        catch(Exception e){
 //           response.getOutputStream().println(e);
        }
    }

    @RequestMapping("/view/{letterId}")
    public void viewScan(@PathVariable("letterId") Integer letterId, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
        Letter letter = letterService.editLetter(letterId);
        BufferedInputStream filein = null;
        BufferedOutputStream outputs = null;
        try {
            InputStream in = new ByteArrayInputStream(letter.getScan());//sb.toString().getBytes("UTF-8"));
            ServletOutputStream out = response.getOutputStream();
            byte[] outputByte = new byte[4096];
//copy binary contect to output stream
            int len = 0;
//            response.setHeader("Content-Length", ""+ letter.getScan().length);
//            response.setContentType("application/force-download");
//            response.setHeader("Content-Disposition","attachment;filename="+letter.getScanname());
//            response.setHeader("Content-Transfer-Encoding", "binary");
            while((len = in.read(outputByte)) > 0)
            {
                out.write(outputByte, 0, len);
            }
            in.close();
            out.flush();
            out.close();
        }
        catch(Exception e){
            //           response.getOutputStream().println(e);
        }
    }

}
